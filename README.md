# beos-juliagenerator

A Julia fractal generator written by myself in autumn of 2000 for BeOS, which was a little bit popular at this time. It also works on Haiku (tested on Beta 2).

# Building

`g++ Main.cpp -lbe -oJulia`
